class storjshare::install inherits storjshare {

  package { ['git', 'python', 'build-essential']:
    ensure => 'installed'
  }

  class { 'nodejs':
    repo_url_suffix => '6.x',
  }

  package { 'storjshare-daemon':
    ensure   => 'present',
    provider => 'npm',
  }

  file { $storjshare::share_dir:
    ensure => directory,
    owner  => $storjshare::owner,
    group  => $storjshare::group
  }

  group { $storjshare::group:
    ensure     => present,
  }

  user { $storjshare::owner:
    ensure     => present,
    groups     => [$storjshare::group],
    password   => '*',
    home       => $storjshare::share_dir,
    require    => Group[$storjshare::group]
  }
}