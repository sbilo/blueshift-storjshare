class storjshare::config inherits storjshare {

  exec { 'storjshare::config::generate':
    command => "/usr/bin/sudo -H -u ${storjshare::owner} /usr/bin/storjshare create --noedit -o ${storjshare::share_dir}/.storjshare --storj ${storjshare::ewallet} --storage ${storjshare::share_dir} --size ${storjshare::storage_mb}MB",
    unless  => "/usr/bin/test -f ${storjshare::share_dir}/.storjshare"
  }
}