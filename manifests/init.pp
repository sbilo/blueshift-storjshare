# Class: storjshare
# ===========================
#
# Full description of class storjshare here.
#
# Authors
# -------
#
# Author Name <sander.bilo@gmail.com>
#
# Copyright
# ---------
#
# Copyright 2017 Sander Bilo, unless otherwise noted.
#
class storjshare(
  Integer $storage_mb               = 1024,
  String $ewallet                   = '0x005463668233189e1a954F5BCbA37605C54b7DFD',
  String $owner                     = 'storjshare',
  String $group                     = 'storjshare',
  Stdlib::AbsolutePath $share_dir   = '/opt/storjshare',
  Boolean $enabled                  = true,
  String $npm_version               = '6.11.2',
) {
  contain storjshare::install
  contain storjshare::service
  contain storjshare::config

  Class['::storjshare::install']
  ~> Class['::storjshare::config']
  ~> Class['::storjshare::service']
}
