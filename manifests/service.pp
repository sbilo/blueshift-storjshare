class storjshare::service inherits storjshare {

  file { '/lib/systemd/system/storjshare-daemon.service':
    ensure  => file,
    content => template('storjshare/storjshare-daemon.service.erb'),
    notify  => Service['storjshare-daemon']
  }

  file { '/lib/systemd/system/storjshare-client.service':
    ensure  => file,
    content => template('storjshare/storjshare-client.service.erb'),
    require => Service['storjshare-daemon'],
  }

  service { 'storjshare-daemon':
    ensure => $storjshare::enabled ? {
      true  => running,
      false => stopped,
    },
    enable => $storjshare::enabled
  }

  service { 'storjshare-client':
    ensure  => $storjshare::enabled ? {
      true  => running,
      false => stopped,
    },
    enable  => $storjshare::enabled,
    require => [Service['storjshare-daemon'],Exec['storjshare::config::generate']]
  }
}